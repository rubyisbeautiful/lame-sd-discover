# lame-sd-discover
Lame basic service discovery - discovery piece

Still very much just a WIP - use haproxy, consul, and consul-template to load balance across containers.

Example:

docker run --env CONSUL_AGENT=192.168.100.1 --env SERVICE_NAME=hello-world --env SERVICE_PORT=8080 -P rubyisbeautiful/lame-sd-discover-auto:latest
